from django.contrib import admin
from .models import (
    Order,
    Trip,
    ItemCategory,
    Item,
    Sla
    
)
# Register your models here.
admin.site.register(Order)
admin.site.register(Trip)
admin.site.register(ItemCategory)
admin.site.register(Item)
admin.site.register(Sla)
