from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework import status
class OrderPagination(PageNumberPagination):
    page_size=1
    page_size_query_param='limit'
    def get_paginated_response(self, data):
        count = self.page.paginator.count
        code = status.HTTP_404_NOT_FOUND
        message_code = 'TRIPLOGISTIC_ORDER_NOT_FOUND'
        message = 'Fail to retrieve triplogistic order'
        if count > 0:
            code  = status.HTTP_200_OK
            message_code = 'TRIPLOGISTIC_ORDER_OK'
            message = 'Success retrieve triplogistic order'
        return Response({
            'status': code,
            'message': message,
            'status_code': message_code,
            'links': {
                'next': self.get_next_link(),
                'previous': self.get_previous_link()
            },
            'total_orders': self.page.paginator.count,
            'triplogistic_orders': data
        },status=code)