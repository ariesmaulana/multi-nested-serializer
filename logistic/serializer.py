from rest_framework import serializers
from .models import (
    Order,
    Trip,
    ItemCategory,
    Item,
    Sla
)

class ItemCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemCategory
        fields = '__all__'

class ItemSerializer(serializers.ModelSerializer):

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if data['category']:
            data['category'] = ItemCategorySerializer(ItemCategory.objects.get(pk=data['category'])).data
        return data

    class Meta:
        model = Item
        fields = '__all__'

class SlaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sla
        exclude = ('trip',)

class TripSerializer(serializers.ModelSerializer):
    sla = SlaSerializer()
    items = ItemSerializer(many=True)
    class Meta:
        model = Trip
        exclude = ('order',)

class OrderSerializer(serializers.ModelSerializer):
    trips = TripSerializer(many=True)

    def validate_trips(self, value):

        if len(value) > 5:
            raise serializers.ValidationError("Maximum items is 5")
        return value

    class Meta:
        model = Order
        fields = ('unique_code','type_order','trips')
    
    def create(self, validated_data):
        trips_data = validated_data.pop('trips')
        order = Order.objects.create(**validated_data)
        for trip_data in trips_data:
            sla = trip_data.pop('sla')
            items = trip_data.pop('items')
            trip = Trip.objects.create(order=order, **trip_data)
            Sla.objects.create(trip=trip,**sla)
            for item in items:
                Item.objects.create(trip=trip,**item)

            
        return order