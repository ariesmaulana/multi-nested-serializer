from rest_framework.generics import ListCreateAPIView
from rest_framework.views import APIView
from .models import Order, Trip
from .serializer import OrderSerializer, TripSerializer
from .pagination import OrderPagination
from rest_framework.response import Response
import random
import uuid
# Create your views here.
class OrderCreateView(ListCreateAPIView):
    queryset=Order.objects.all()
    serializer_class=OrderSerializer
    pagination_class=OrderPagination

    def post(self, request, *args, **kwargs):
        unique_code = randomString(7)
        request.data['unique_code'] = unique_code
        return self.create(request, *args, **kwargs)
    
    # def get_queryset(self):
    #     return Order.objects.all()

class TripCreateView(ListCreateAPIView):
    queryset=Trip.objects.all()
    serializer_class = TripSerializer


def randomString(stringLength=10):
    """Generate a random string of fixed length """
    """Returns a random string of length string_length."""
    random = str(uuid.uuid4())
    random = random.upper()
    random = random.replace("-","")
    return random[0:stringLength]

#Example Payload
# {
#   "type_order": "REGULAR",
#   "trips": [
#     {
#       "origin_pic": "sender",
#       "origin_phone": "123",
#       "origin_address": "some street",
#       "destination_pic": "receiver",
#       "destination_phone": "123",
#       "destination_address": "some destination",
#       "sla": {
#         "vehicle": "bike",
#         "group": "Instant",
#         "description": "3-4 hours",
#         "price": "10000"
#       },
#       "items": [
#         {
#           "category": "1",
#           "name": "PC",
#           "weight": "10",
#           "dimension": "2x3x4"
#         },
#         {
#           "category": "1",
#           "name": "PC",
#           "weight": "10",
#           "dimension": "2x3x4"
#         }
#       ]
#     },
#     {
#       "origin_pic": "sender",
#       "origin_phone": "123",
#       "origin_address": "some street",
#       "destination_pic": "receiver 2",
#       "destination_phone": "123",
#       "destination_address": "some destination 2",
#       "sla": {
#         "vehicle": "bike",
#         "group": "Instant",
#         "description": "3-4 hours",
#         "price": "10000"
#       },
#       "items": [
#         {
#           "category": "1",
#           "name": "PC",
#           "weight": "10",
#           "dimension": "2x3x4"
#         }
#       ]
#     }
#   ]
# }