from django.db import models

# Create your models here.
class Order(models.Model):
    unique_code=models.CharField(max_length=200, unique=True)
    type_order=models.CharField(max_length=200, null=True)
    created_at=models.DateTimeField(auto_now_add=True)
    updated_at=models.DateTimeField(auto_now=True)
    def __str__(self):
        return self.unique_code

class Trip(models.Model):
    order=models.ForeignKey(Order, on_delete=models.SET_NULL, null=True, related_name='trips')
    origin_pic=models.CharField(max_length=255)
    origin_phone=models.CharField(max_length=12)
    origin_address=models.TextField()
    destination_pic=models.CharField(max_length=255)
    destination_phone=models.CharField(max_length=12)
    destination_address=models.TextField()
    created_at=models.DateTimeField(auto_now_add=True)
    updated_at=models.DateTimeField(auto_now=True)
    def __str__(self):
        return self.order

class ItemCategory(models.Model):
    name=models.CharField(max_length=255)
    description=models.TextField()
    created_at=models.DateTimeField(auto_now_add=True)
    updated_at=models.DateTimeField(auto_now=True)
    def __str__(self):
        return self.name

class Item(models.Model):
    trip=models.ForeignKey(Trip, on_delete=models.SET_NULL, null=True, related_name='items')
    category=models.ForeignKey(ItemCategory, on_delete=models.SET_NULL, null=True)
    name=models.CharField(max_length=255)
    weight=models.IntegerField()
    dimension=models.TextField()

class Sla(models.Model):
    trip=models.OneToOneField(Trip, on_delete=models.SET_NULL, null=True, related_name='sla')
    vehicle=models.CharField(max_length=200)
    group=models.CharField(max_length=200)
    description=models.TextField()
    price= models.DecimalField(max_digits=10, decimal_places=2)
    