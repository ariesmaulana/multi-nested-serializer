from django.urls import path
from . import views

app_name = 'logistic'
urlpatterns = [
    path('', views.OrderCreateView.as_view(), name='index'),
    path('trip/',views.TripCreateView.as_view(), name='trip'),
]